const validateForEmptyFields = (...args) => {
    const fieldsArray = [...args];

    const hasEmptyFields = fieldsArray.some(field => field.value === '');

    return hasEmptyFields;
}

export const useFieldsValidator = () => ({
    validateForEmptyFields
})

