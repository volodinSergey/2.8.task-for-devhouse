const randomId = Math.random()

export const useId = () => ({
    randomId,
})