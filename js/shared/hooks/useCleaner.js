const cleanFieldValue = (...args) => {
    const inputsArray = [...args]

    if (inputsArray.length === 1) {
        inputsArray[0].value = ''

        return
    }

    inputsArray.forEach(input => input.value = '')
}

const cleanDomListBeforeRender = list => list.innerHTML = ''

export const useCleaner = () => ({
    cleanFieldValue,
    cleanDomListBeforeRender
})