import { useDom, useActiveClasses } from "./burger.dom.js"

export const initBurger = () => {
    const { burger, menu } = useDom()
    const { menuOpenedSelector, activeBurgerItemSelector } = useActiveClasses()

    const toggleMenu = () => menu.classList.toggle(menuOpenedSelector)

    function toggleBurgerItems(burger) {
        const burgerItems = [...burger.children]

        burgerItems.forEach(item => item.classList.toggle(activeBurgerItemSelector))
    }

    burger.addEventListener('click', () => {
        toggleMenu()
        toggleBurgerItems(burger)
    })
}