const domState = {
    burgerSelector: '.burger',
    menuSelector: '.menu',

    menuOpenedSelector: 'opened',
    activeBurgerItemSelector: 'burger__item-color-when-menu-opened',
}
const { menuOpenedSelector, activeBurgerItemSelector } = domState

const burger = document.querySelector(domState.burgerSelector)
const menu = document.querySelector(domState.menuSelector)

export const useDom = () => ({
    burger,
    menu,
})

export const useActiveClasses = () => ({
    menuOpenedSelector,
    activeBurgerItemSelector,
})