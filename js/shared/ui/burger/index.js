import { initBurger } from "./burger.initBurger.js"

const burgerModule = () => initBurger()

export { burgerModule }
