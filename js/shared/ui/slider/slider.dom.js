const domState = {
    bigSlideSelector: '.slide-box__slide',
    thumbsListSelector: '.slider-thumbs',
    thumbSelector: 'slider-thumbs__thumb',
    thumbImageSelector: 'slider-thumbs__thumb-image',
    thumbDescriptionSelector: '.slide-info__description',
    prevButtonSelector: '.button-prev',
    nextButtonSelector: '.button-next',

    activeThumbSelector: 'active-thumb',
}

const thumbsList = document.querySelector(domState.thumbsListSelector)
const thumbDescription = document.querySelector(domState.thumbDescriptionSelector)
const bigSlide = document.querySelector(domState.bigSlideSelector)

const prevButton = document.querySelector(domState.prevButtonSelector)
const nextButton = document.querySelector(domState.nextButtonSelector)

const { thumbSelector, activeThumbSelector, thumbImageSelector } = domState

export const useDom = () => ({
    thumbsList,
    thumbSelector,
    thumbDescription,
    thumbImageSelector,
    bigSlide,
    prevButton,
    nextButton,
})


export const useActiveClasses = () => ({
    activeThumbSelector
})