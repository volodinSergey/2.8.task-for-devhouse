import { thumbElements } from './slider.render.js'

import { useDom, useActiveClasses } from './slider.dom.js'
import { initActiveThumb, initActiveBigSlide } from './slider.init.js'

export const buttonsBehavior = () => {
    const { prevButton, nextButton } = useDom()
    const { activeThumbSelector } = useActiveClasses()

    let slideIndex = 0

    const moveUp = () => {
        slideIndex += 1

        if (slideIndex > thumbElements.length - 1) slideIndex = 0

        thumbElements.forEach(slide => slide.classList.remove(activeThumbSelector))

        initActiveThumb(slideIndex, thumbElements)
        initActiveBigSlide(slideIndex, thumbElements)
    }

    const moveBack = () => {
        slideIndex -= 1

        if (slideIndex < 0) slideIndex = thumbElements.length - 1

        initActiveThumb(slideIndex, thumbElements)
        initActiveBigSlide(slideIndex, thumbElements)
    }

    prevButton.addEventListener('click', moveBack)
    nextButton.addEventListener('click', moveUp)
}