import sliderData from "./slider.data.js"
import { useDom } from "./slider.dom.js"

const { thumbsList, thumbSelector, thumbImageSelector } = useDom()

const render = (sliderData, whereToRender) => {
    const slidesElementsArray = sliderData.map(({ src, description }) => {
        const liAndImageInside = document.createElement('li')
        liAndImageInside.classList.add(thumbSelector)

        const newSlide = document.createElement('img')

        newSlide.classList.add(thumbImageSelector)
        newSlide.src = src
        newSlide.alt = 'thumb'
        newSlide.setAttribute('data-description', description)

        liAndImageInside.appendChild(newSlide)

        return liAndImageInside
    })

    const thumbsArray = slidesElementsArray.map(item => item.firstChild)
    thumbsArray.forEach(slide => whereToRender.appendChild(slide))


}

render(sliderData, thumbsList)

export const thumbElements = [...thumbsList.children]

