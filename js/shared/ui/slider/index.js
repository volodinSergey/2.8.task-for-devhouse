import { initActiveThumbAndBigSlideByDefault, onClickThumbBehavior } from "./slider.init.js"

import { buttonsBehavior } from "./slider.buttons.js"

export const sliderModule = () => {
    initActiveThumbAndBigSlideByDefault()

    onClickThumbBehavior()

    buttonsBehavior()
}
