import { useDom, useActiveClasses } from './slider.dom.js'
import { thumbElements } from './slider.render.js'

const { thumbsList, thumbDescription, bigSlide } = useDom()
const { activeThumbSelector } = useActiveClasses()

export const initActiveThumb = (slideIndex, thumbElements) => {
    thumbElements.forEach(thumb => thumb.classList.remove(activeThumbSelector))

    thumbElements[slideIndex].classList.add(activeThumbSelector)

    thumbDescription.textContent = thumbElements[slideIndex].dataset.description
}

export const initActiveBigSlide = (slideIndex = 0, thumbElements) => bigSlide.src = thumbElements[slideIndex].src

export const initActiveThumbAndBigSlideByDefault = () => {
    initActiveThumb(0, thumbElements)
    initActiveBigSlide(0, thumbElements)
}


export const onClickThumbBehavior = () => {
    thumbsList.addEventListener('click', (e) => {
        if (!e.target.classList.contains('slider-thumbs__thumb-image')) return

        const currentThumbImage = e.target

        bigSlide.src = currentThumbImage.src

        thumbElements.forEach(thumb => thumb.classList.remove(activeThumbSelector))

        currentThumbImage.classList.add(activeThumbSelector)

        thumbDescription.textContent = currentThumbImage.dataset.description
    })
}