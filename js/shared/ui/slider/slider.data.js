const sliderData = [
    {
        src: '../../assets/images/sliderContent/moscow.slide.jpg',
        description: 'This is Moscow',
    },

    {
        src: '../../assets/images/sliderContent/new-york.slide.jpg',
        description: 'This is New York',
    },

    {
        src: '../../assets/images/sliderContent/oslo.slide.jpg',
        description: 'This is Oslo',
    },

    {
        src: '../../assets/images/sliderContent/rim.slide.jpg',
        description: 'This is Rim',
    },

    {
        src: '../../assets/images/sliderContent/tokio.slide.jpg',
        description: 'This is Tokio',
    },

    {
        src: '../../assets/images/sliderContent/Ilon.jpg',
        description: 'This is funny Ilon. He is smiling at you :)',
    },
]

export default sliderData