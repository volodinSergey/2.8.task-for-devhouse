import commentsData from "./comments.backend.js";
import { useDom } from "./comments.dom.js";

const { commentsList } = useDom()

const WHEN_NO_СONTENT_TEXT = 'Нет комментариев'

const checkForNoComments = () => (!commentsData.length) && (commentsList.textContent = WHEN_NO_СONTENT_TEXT)

const render = () => {
    const commentsArray = commentsData.map(({ avatar, text, subtext }) => {
        const template = document.getElementById('comment-template')

        const commentAvatar = template.content.querySelector('.comments-list__item-avatar')
        const commentText = template.content.querySelector('.comments-list__item-info_text')
        const commentSubtext = template.content.querySelector('.comments-list__item-info_subtext')

        commentAvatar.src = avatar
        commentText.textContent = text
        commentSubtext.textContent = subtext

        const li = template.content.cloneNode(true)

        return li
    })

    commentsArray.forEach(item => commentsList.appendChild(item))
}

export { checkForNoComments, render }