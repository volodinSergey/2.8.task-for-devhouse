import { render } from './comments.render.js'

import { useDom } from './comments.dom.js'
import { useCleaner } from '../shared/hooks/useCleaner.js'
import { useFieldsValidator } from '../shared/hooks/useFieldsValidator.js'
import { newCommentFactory } from './comments.factory.js'

import CommentsService from '../infrastructure/services/Comments.service.js'
import TextEditorService from '../shared/helpServices/TextEditor.js'

const { commentsList, textField, subTextField } = useDom()

const { validateForEmptyFields } = useFieldsValidator()
const { cleanFieldValue, cleanDomListBeforeRender } = useCleaner()

const commentsService = new CommentsService()
const textEditor = new TextEditorService()

const onAddComment = () => {
    if (validateForEmptyFields(textField, subTextField)) {
        alert('Заполните все поля!')

        return
    }

    const normalizedTextField = textEditor.take(textField.value).deleteGaps().getEditedText()
    const normalizedSubTextField = textEditor.take(subTextField.value).deleteGaps().getEditedText()

    const newComment = newCommentFactory(normalizedTextField, normalizedSubTextField)

    commentsService.addComment(newComment)

    cleanFieldValue(textField, subTextField)
    cleanDomListBeforeRender(commentsList)

    render()
}

export const useCommentStore = () => ({
    onAddComment
}) 