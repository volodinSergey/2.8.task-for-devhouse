import { useId } from '../shared/hooks/useId.js'

const { randomId } = useId()

export const newCommentFactory = (textForAdding, subtextForAdding) => ({
    id: randomId,
    avatar: '../../assets/images/Ilon.jpg',
    text: textForAdding,
    subtext: subtextForAdding,
})