const domState = {
    textFieldSelector: '.comments-form__input',
    subtextFieldSelector: '.comments-form__field',
    addCommentButtonSelector: '.comments-form__button',

    commentsListSelector: '.comments-list',
}

const textField = document.querySelector(domState.textFieldSelector)
const subTextField = document.querySelector(domState.subtextFieldSelector)
const addCommentButton = document.querySelector(domState.addCommentButtonSelector)

const commentsList = document.querySelector(domState.commentsListSelector)

export const useDom = () => ({
    textField,
    subTextField,
    addCommentButton,

    commentsList
})