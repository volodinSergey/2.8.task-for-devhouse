import { useDom } from "./comments.dom.js"
import { useCommentStore } from "./comments.store.js"

import { checkForNoComments, render } from "./comments.render.js"

export const commentsModule = () => {
    checkForNoComments() || render()

    const { addCommentButton } = useDom()
    const { onAddComment } = useCommentStore()

    addCommentButton.addEventListener('click', onAddComment)
}