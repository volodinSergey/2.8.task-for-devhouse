'use strict'

import { burgerModule } from '../../js/shared/ui/burger/index.js'
import { sliderModule } from '../../js/shared/ui/slider/index.js'
import { commentsModule } from '../../js/commentsModule/index.js'

document.addEventListener('DOMContentLoaded', () => {
    burgerModule()

    sliderModule()

    commentsModule()
})