'use strict'

import { burgerModule } from '../../js/shared/ui/burger/index.js'

document.addEventListener('DOMContentLoaded', () => {
    burgerModule()
})